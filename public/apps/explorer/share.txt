<body>
<p>Hi,</p>
<p>The file or directory '[FILE]' has been shared with you. Click <a href="[LINK]">this link</a> to access it. The link expires on [EXPIRE].[PASSWORD]</p>
<p>[COMMENT]</p>
<p>Kind regards,<br />[USERNAME]</p>
</body>
