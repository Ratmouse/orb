Build CKEditor 5 for Orb
------------------------

Go to https://ckeditor.com/ckeditor-5/online-builder/.

Select Classic and select the following options:
- AI Assistant: Remove
- Alignment: Add
- Auto image: Add
- Autoformat: Remove
- Autolink: Add
- Autosave: Remove
- Base64 upload adapter: Remove
- Block image: Add
- Block quote: Add
- Bold: Add
- CKBox: Remove
- CKFinder: Remove
- CKFinder upload adapter: Remove
- Cloud Services: Remove
- Code: Add
- Code blocks: Add
- Comments: Remove
- Data filter: Add
- Data schema: Add
- Document list: Remove
- Document list properties: Remove
- Document outline: Remove
- Easy image: Remove
- Export to PDF: Remove
- Export to Word: Remove
- Find and replace: Add
- Font background color: Add
- Font color: Add
- Font family: Add
- Font size: Add
- Format painter: Remove
- General HTML support: Add
- Heading: Add
- Highlight: Add
- Horizontal line: Add
- HTML comment: Remove
- HTML embed: Add
- Image: Add
- Image caption: Add
- Image insert: Add
- Image resize: Add
- Image style: Add
- Image toolbar: Add
- Image upload: Add
- Import from Word: Remove
- Indent: Add
- Indent block: Add
- Inline image: Add
- Italic: Add
- Link: Add
- Link image: Add
- List: Add
- List properties: Add
- Markdown: Remove
- MathType: Remove
- Media embed: Add
- Media embed toolbar: Add
- Mention: Remove
- Page break: Remove
- Pagination: Remove
- Paste from Office: Add
- Presence list: Remove
- Real-time collaborative comments: Remove
- Real-time collaborative editing: Remove
- Real-time collaborative revision history: Remove
- Real-time collaborative track changes: Remove
- Remove format: Add
- Restricted editing mode: Remove
- Revision History: Remove
- Select all: Add
- Show blocks: Remove
- Simple upload adapter: Remove
- Slash command: Remove
- Source editing: Add
- Special characters: Add
- Special characters arrows: Add
- Special characters currency: Add
- Special characters essentials: Add
- Special characters latin: Add
- Special characters mathematical: Add
- Special characters text: Add
- Standard editing mode: Remove
- Strikethrough: Add
- Style: Add
- Subscript: Add
- Superscript: Add
- Table: Add
- Table caption: Add
- Table cell properties: Add
- Table column resize: Add
- Table of contents: Remove
- Table properties: Add
- Table toolbar: Add
- Template: Remove
- Text part language: Remove
- Text transformation: Add
- Title: Remove
- To-do list: Remove
- Track changes: Remove
- Track changes data: Remove
- Underline: Add
- Watchdog: Remove
- Word count: Add
- WProofreader: Remove

Ignore the toolbar item selection and click 'Next step'.

Select English as the default editor language.

Download the custom CKeditor build.

Extract the file ckeditor.js from the 'build' directory inside the downloaded
ZIP file and copy it to public/js/banshee inside your Banshee installation.
