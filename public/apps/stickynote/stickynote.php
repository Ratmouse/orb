<?php
	namespace Orb;

	if (defined("ORB_VERSION") == false) exit;

	class stickynote extends orb_backend {
		private $notes_file = null;
		private $notes = null;

		public function execute() {
			$this->notes_file = $this->home_directory."/.sticky_notes";

			if (file_exists($this->notes_file) == false) {
				$this->notes = array();
			} else {
				$notes = file_get_contents($this->notes_file);
				$this->notes = json_decode($notes, true);
			}

			parent::execute();
		}

		public function get_load() {
			foreach ($this->notes as $index => $note) {
				$note["index"] = $index;
				$this->view->record($note, "note");
			}
		}

		private function index($data) {
			if (isset($data["index"])) {
				return (int)$data["index"];
			} else if (count($this->notes) == 0) {
				return 0;
			} else {
				return max(array_keys($this->notes)) + 1;
			}
		}

		private function save_notes() {
			if (($fp = fopen($this->notes_file, "w")) == false) {
				return false;
			}

			fputs($fp, json_encode($this->notes));

			fclose($fp);

			return true;
		}

		public function post_save() {
			$index = $this->index($_POST);

			$this->notes[$index] = array(
				"x"    => $_POST["x"],
				"y"    => $_POST["y"],
				"text" => $_POST["text"]);

			$this->view->add_tag("index", $index);

			return $this->save_notes();
		}

		public function post_delete() {
			$index = $this->index($_POST);

			unset($this->notes[$index]);

			return $this->save_notes();
		}
	}
?>
