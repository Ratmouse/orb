/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const USER_LOGIN_SCRIPT = 'login.ujs';
const USER_LOAD_SCRIPT = 'load.ujs';

function javascript_execute_file(filename) {
	orb_file_exists(filename, function(exists) {
		if (exists == false) {
			orb_alert('Javascript not found.');
			return;
		}

		var js_id = 'js_' + sha256(filename);

		filename = '/orb/file/download/' + filename;

		if ($('div.desktop').attr('debug') == 'yes') {
			filename += '?' + Date.now();
		}

		$('head script#' + js_id).remove();
		$('head').append('<script id=\"' + js_id + '\" type="text/javascript" src="' + filename + '"></script>');
	});
}

$(document).ready(function() {
	orb_upon_file_open('ujs', javascript_execute_file, '/images/application.png');

	/* Check login and load scripts
	 */
	if (parseInt($('div.desktop').attr('counter')) == 0) {
		orb_file_exists(USER_LOGIN_SCRIPT, function(exists) {
			if (exists) {
				javascript_execute_file(USER_LOGIN_SCRIPT);
			}
		});
	}

	orb_file_exists(USER_LOAD_SCRIPT, function(exists) {
		if (exists) {
			javascript_execute_file(USER_LOAD_SCRIPT);
		}
	});
});
